lexer grammar KaminLexer;

// keywords
Return: 'return';
If: 'if';
Else: 'else';
Elif: 'elif';
For: 'for';
Break: 'break';
Continue: 'continue';
While: 'while';
Struct: 'struct';

Dollar: '$';
OpenSqbr: '[';
CloseSqbr: ']';
Plus: '+';
Minus: '-';
Slash: '/';
Gt: '>';
Lt: '<';
Ge: '>=';
Le: '<=';
Eq: '==';
Ne: '!=';
At: '@';
Hash: '#';
Dot: '.';
Assignment: '=';
Arrow: '->';
Asterisk: '*';
Colon: ':';
OpenParen: '(';
CloseParen: ')';
Comma: ',';
Semicolon: ';';
OpenBracket: '{';
CloseBracket: '}';

/* fragment Escaped: '\\' [abfnrtv\\'"]; */
/* String: '"' (~["\\] | Escaped)* '"'; */
String: '"' ~["]* '"';
Float: '-'? [0-9]+ '.' [0-9]+;
Int: '-'? [0-9]+;

Ident: [a-zA-Z0-9_]+;
Comment: '//' ~[\r\n]* -> channel(HIDDEN);
Whitespace: [ \r\n\t]+ -> channel(HIDDEN);

