#!/bin/sh

set -xe

antlr4_jar="./antlr4/tool/target/antlr4-4.13.2-SNAPSHOT-complete.jar"

java -jar $antlr4_jar -Dlanguage=Python3 -no-listener -visitor -o generated KaminLexer.g4
java -jar $antlr4_jar -Dlanguage=Python3 -no-listener -visitor -o generated KaminParser.g4
