import argparse
import os
from pathlib import Path
import inspect
from generated.KaminLexer import KaminLexer
from generated.KaminParser import KaminParser
from generated.KaminParserVisitor import KaminParserVisitor
import antlr4
from subprocess import Popen, PIPE, STDOUT
import sys
import copy

STD_PATH = None

def error(reason, input_file_info=None):
    info = inspect.getframeinfo(inspect.currentframe().f_back)
    print(f"{Path(info.filename).absolute()}:{info.lineno}: {reason}")
    if input_file_info:
        print(f"    in file {input_file_info[0]}:{input_file_info[1]}:{input_file_info[2]}")

def source_file_to_module_name(path):
    cwd = os.getcwd()
    expandable_prefixes = ["root", "std"]
    for expandable_prefix in expandable_prefixes:
        root = None
        if expandable_prefix == "root":
            root = cwd
        elif expandable_prefix == "std":
            root = STD_PATH

        if path.split(os.sep)[0] == expandable_prefix:
            use = True
            new_path_components = path.split(os.sep)
            new_path_components[0] = root
            path = os.sep.join(new_path_components)
            parent = str(Path(root).parent.absolute()) + os.sep
            module_name = os.path.splitext(path)[0]
            module_name = module_name.replace(parent, "")
            module_name = module_name.replace(os.sep, ".")
            return (module_name, path)

def rule_get_full_text(ctx):
    src = ctx.start.getTokenSource()
    stream = src.inputStream
    start, stop = ctx.start.start, ctx.stop.stop
    return stream.getText(start, stop)

def kamin_to_c_name(name):
    name = name.replace(".", "_")
    name = name.replace("*", "ptr")
    return name

class Type:
    def __init__(self, text, base_name, c_type):
        self.text = text
        self.base_name = base_name
        self.c_type = c_type

    def __str__(self):
        return self.c_type

class StructField:
    def __init__(self, name, type_):
        self.name = name
        self.type_ = type_

class StructTypeBase:
    def __init__(self, text, fields):
        self.text = text
        self.fields = fields

class GenericStructTypeBase:
    def __init__(self, text, generic_names, fields):
        self.text = text
        self.generic_names = generic_names
        self.fields = fields

class StructType(Type):
    def __init__(self, text, fields, base_name, c_type, is_from_generic):
        self.fields = fields
        self.is_from_generic = is_from_generic
        super().__init__(text, base_name, c_type)
        self.inst_text = self.make_inst_text()

    def inst_postfix(self):
        return "_" + "_".join([field.type_.c_type for field in self.fields])

    def make_inst_text(self):
        c_type = kamin_to_c_name(self.base_name)
        if self.is_from_generic:
            c_type += self.inst_postfix()
        fields = ";\n".join([field.type_.c_type + " " + field.name for field in self.fields]) + ";"
        c_type = c_type.replace("*", "ptr")
        return f"typedef struct {{\n{fields}\n}} {c_type};"

class GenericStructType(Type):
    def __init__(self, text, generic_names, fields, name):
        self.name = name
        self.generic_names = generic_names
        self.fields = fields
        super().__init__(text, "", "<cannot translate directly>")

class FuncTypeParam:
    def __init__(self, name, type_, is_va=False):
        self.name = name
        self.type_ = type_
        self.is_va = is_va

class FuncType(Type):
    def __init__(self, text, return_type, params):
        self.return_type = return_type
        self.params = params
        super().__init__(text, "", "<cannot translate directly>")

class GenericFuncType(Type):
    def __init__(self, text, generic_names, return_type, params):
        self.generic_names = generic_names
        self.return_type = return_type
        self.params = params
        super().__init__(text, "", "<cannot translate directly>")

class Function:
    def __init__(self, name, c_name, type_, decl, defn):
        self.name = name
        self.c_name = c_name
        self.type_ = type_
        self.decl = decl
        self.defn = defn
        self.scope_stack = []

class GenericFunction:
    def __init__(self, name, type_, body_ctx):
        self.name = name
        self.type_ = type_
        self.body_ctx = body_ctx

class Expression:
    def __init__(self, c_expr, type_):
        self.c_expr = c_expr
        self.type_ = type_

class StringExpression(Expression):
    def __init__(self, c_expr, type_, string):
        self.string = string
        super().__init__(c_expr, type_)

class LocalVar:
    def __init__(self, name, type_, expr):
        self.name = name
        self.type_ = type_
        self.expr = expr

class GlobalVar:
    def __init__(self, name, c_name, type_, expr, is_const):
        self.name = name
        self.c_name = c_name
        self.type_ = type_
        self.expr = expr
        self.is_const = is_const
        self.text = self.make_text()

    def make_text(self):
        if self.is_const:
            if self.expr == None:
                error("constants must have an expression")
                exit(1)
            return f"#define {self.c_name} {self.expr.c_expr}\n"
        else:
            if self.expr != None:
                expr = self.expr.c_expr
                return f"{self.type_.c_type} {self.c_name} = {expr};"
            else:
                return f"{self.type_.c_type} {self.c_name};"

class Visitor(KaminParserVisitor):
    def __init__(self, current_file, module):
        self.current_file = current_file
        self.module = module
        self.type_map = {
            "nothing": Type("nothing", "nothing", "void"),
            "i8": Type("i8", "i8", "int8_t"),
            "i32": Type("i32", "i32", "int32_t"),
            "i64": Type("i64", "i64", "int64_t"),
            "f32": Type("f32", "f32", "float"),
            "f64": Type("f64", "f64", "double"),
            "u8": Type("u8", "u8", "uint8_t"),
            "u32": Type("u32", "u32", "uint32_t"),
            "u64": Type("u64", "u64", "uint64_t"),
            "boolean": Type("boolean", "boolean", "bool"),
            "raw_str": Type("raw_str", "raw_str", "char*"),
            "c_int": Type("c_int", "c_int", "int"),
            "c_char": Type("c_char", "c_char", "char"),
            "any": Type("any", "any", "void*")
        }
        self.functions = []
        self.generic_functions = []
        self.globalvars = []
        self.custom_types = []
        self.generic_custom_types = []
        self.scope_stack = [{}]
        self.generic_type_map = {}
        self.c_file_string = ""
        self.directive_stack = []
        self.in_macro = False
        self.builtin_functions = {
            "import": self.builtin_import,
            "import_c": self.builtin_import_c,
            "using": self.buiiltin_using,
            "make_include": self.builtin_make_include,
            "make_ifdef": self.builtin_make_ifdef,
            "make_endif": self.builtin_make_endif,
            "make_if": self.builtin_make_if,
            "make_else": self.builtin_make_else,
        }

    def __str__(self):
        custom_types = "\n".join([type_.inst_text for type_ in self.custom_types])
        function_decls = "\n".join([func.decl for func in self.functions])
        function_defns = "\n".join([func.defn for func in self.functions])
        globalvars = "\n".join([globalvar.text for globalvar in self.globalvars])
        c_module_name = kamin_to_c_name(self.module)
        self.c_file_string += f"""
#ifndef {c_module_name}_kamin
#define {c_module_name}_kamin

{custom_types}

{globalvars}

{function_decls}

{function_defns}

#endif // {c_module_name}_kamin
"""
        return self.c_file_string

    def make_scope(self):
        self.scope_stack.append({})

    def drop_scope(self):
        return self.scope_stack.pop()

    def visitFile(self, ctx:KaminParser.FileContext):
        for statement in ctx.statement():
            self.visitStatement(statement)
    
    def visitStatement(self, ctx:KaminParser.StatementContext):
        statement = None
        if ctx.globalDecl():
            statement = self.visitGlobalDecl(ctx.globalDecl())
        elif ctx.assignment():
            statement = self.visit(ctx.assignment())
        elif ctx.expression():
            c_expr = self.visit(ctx.expression()).c_expr
            if len(c_expr) == 0:
                statement = ""
            else:
                statement = c_expr + ";"
        elif ctx.reassignment():
            statement = self.visit(ctx.reassignment())
        elif ctx.returnStatement():
            statement = self.visit(ctx.returnStatement())
        elif ctx.forStatement():
            statement = self.visit(ctx.forStatement())
        elif ctx.ifStatement():
            statement = self.visit(ctx.ifStatement())
        elif ctx.breakStatement():
            statement = self.visit(ctx.breakStatement())
        elif ctx.continueStatement():
            statement = self.visit(ctx.continueStatement())
        elif ctx.whileStatement():
            statement = self.visit(ctx.whileStatement())
        elif ctx.expressionAssignment():
            statement = self.visit(ctx.expressionAssignment())

        if statement == None:
            error(f"TODO: unhandled statement {rule_get_full_text(ctx)}", (self.current_file, ctx.start.line, ctx.start.column))
            exit(1)
        if self.in_macro:
            self.c_file_string += statement
        else:
            return statement
    
    def visitWhileBody(self, ctx:KaminParser.WhileBodyContext):
        return "\n".join([("  " * (len(self.scope_stack)-1)) + self.visit(statement) for statement in ctx.statement()])
    
    def visitWhileStatement(self, ctx:KaminParser.WhileStatementContext):
        cond = self.visit(ctx.expression())
        self.make_scope()
        statements = self.visit(ctx.whileBody())
        self.drop_scope()
        spaces = "  " * (len(self.scope_stack) - 1)
        return f"while ({cond.c_expr}) {{\n{statements}\n{spaces}}}"
       
    def visitForBody(self, ctx:KaminParser.ForBodyContext):
        return "\n".join([("  " * (len(self.scope_stack)-1)) + self.visit(statement) for statement in ctx.statement()])
    
    def visitForStatement(self, ctx:KaminParser.ForStatementContext):
        self.make_scope()

        first = self.visit(ctx.statement()[0])
        cond = self.visit(ctx.statement()[1])
        final = self.visit(ctx.statement()[2])

        self.make_scope()
        statements = self.visit(ctx.forBody())
        self.drop_scope()

        self.drop_scope()
        spaces = "  " * (len(self.scope_stack) - 1)
        return f"for ({first} {cond} {final[:-1]}) {{\n{statements}\n{spaces}}}"
    
    def visitElifBody(self, ctx:KaminParser.ElifBodyContext):
        statements = "\n".join([("  " * (len(self.scope_stack)-1)) + self.visit(statement) for statement in ctx.statement()])
        cond = self.visit(ctx.expression())
        return statements, cond
    
    def visitElseBody(self, ctx:KaminParser.ElseBodyContext):
        return "\n".join([("  " * (len(self.scope_stack)-1)) + self.visit(statement) for statement in ctx.statement()])
    
    def visitIfBody(self, ctx:KaminParser.IfBodyContext):
        return "\n".join([("  " * (len(self.scope_stack)-1)) + self.visit(statement) for statement in ctx.statement()])
    
    def visitIfStatement(self, ctx:KaminParser.IfStatementContext):
        self.make_scope()
        cond = self.visit(ctx.expression())
        statements = self.visit(ctx.ifBody())
        self.drop_scope()
        spaces = "  " * (len(self.scope_stack) - 1)
        text = f"if ({cond.c_expr}) {{\n{statements}\n{spaces}}}"

        for elif_ in ctx.elifBody():
            self.make_scope()
            elif_statements, elif_cond = self.visit(elif_)
            text += f" else if ({elif_cond.c_expr}) {{\n{elif_statements}\n{spaces}}}"
            self.drop_scope()

        if ctx.elseBody():
            self.make_scope()
            else_statements = self.visit(ctx.elseBody())
            text += f" else {{\n{else_statements}\n{spaces}}}"
            self.drop_scope()

        return text

    # TOOD: implement return type checking
    def visitReturnStatement(self, ctx:KaminParser.ReturnStatementContext):
        if ctx.expression():
            return f"return {self.visit(ctx.expression()).c_expr};"
        else:
            return f"return;"
    
    def visitBreakStatement(self, ctx:KaminParser.BreakStatementContext):
        return "break;"
    
    def visitContinueStatement(self, ctx:KaminParser.ContinueStatementContext):
        return "continue;"
    
    def visitReassignment(self, ctx:KaminParser.ReassignmentContext):
        name = ".".join([ident.getText() for ident in ctx.Ident()])
        expr = self.visit(ctx.expression())
        for frame in self.scope_stack:
            if name in frame:
                if frame[name].type_.c_type != expr.type_.c_type:
                    error(f"cannot assign expression of type `{expr.type_.text}` to local variable {name} of type `{frame[name].type_.text}`")
                    exit(1)
                return f"{name} = {expr.c_expr};"
        global_name = self.module + "." + name
        for globalvar in self.globalvars:
            if globalvar.name == global_name:
                name = global_name
            if globalvar.name == name:
                if globalvar.type_.c_type != expr.type_.c_type:
                    error(f"cannot assign expression of type `{expr.type_.text}` to global variable {name} of type `{globalvar.type_.text}`")
                    exit(1)
                return f"{kamin_to_c_name(name)} = {expr.c_expr};"
        error(f"tried to assign an undeclared variable `{name}`", (self.current_file, ctx.start.line, ctx.start.column))
        exit(1)
    
    def visitAssignment(self, ctx:KaminParser.AssignmentContext):
        name = ctx.Ident().getText()
        expr = self.visit(ctx.expression())
        type_ = self.visit(ctx.typeName()) if ctx.typeName() else expr.type_

        if expr.type_.c_type != type_.c_type:
            error(f"cannot assign type `{type_.text}` to type `{expr.type_.text}`", (self.current_file, ctx.start.line, ctx.start.column))
            exit(1)

        if name in self.scope_stack[-1]:
            error(f"`{name}` is already available in this scope")
            exit(1)

        self.scope_stack[-1][name] = LocalVar(name, type_, expr)
        return f"{type_} {name} = {expr.c_expr};"
    
    def visitExpressionAssignment(self, ctx:KaminParser.ExpressionAssignmentContext):
        expr1 = self.visit(ctx.expression()[0])
        expr2 = self.visit(ctx.expression()[1])
        if expr1.type_.c_type != expr2.type_.c_type:
            error("LHS and RHS of assignment have different types ({expr1.type_.text} and {expr2.type_.text})",
                  (self.current_file, ctx.start.line, ctx.start.column))
            exit(1)
        return f"{expr1.c_expr} = {expr2.c_expr};"
    
    def visitIntExpression(self, ctx:KaminParser.IntExpressionContext):
        if ctx.typeName():
            type_ = self.visit(ctx.typeName())
        else:
            type_ = self.type_map["i32"]
        return Expression(ctx.Int().getText(), type_)
    
    def visitFloatExpression(self, ctx:KaminParser.FloatExpressionContext):
        if ctx.typeName():
            type_ = self.visit(ctx.typeName())
        else:
            type_ = self.type_map["f32"]
        return Expression(ctx.Float().getText(), type_)
    
    def visitEmphExpression(self, ctx:KaminParser.EmphExpressionContext):
        return self.visit(ctx.expression())
    
    def visitIdentExpression(self, ctx:KaminParser.IdentExpressionContext):
        name = ".".join([ident.getText() for ident in ctx.Ident()])
        for frame in self.scope_stack:
            if name in frame:
                localvar = frame[name]
                return Expression(kamin_to_c_name(name), localvar.type_)
        global_name = self.module + "." + name
        for globalvar in self.globalvars:
            if globalvar.name == global_name or globalvar.name == name:
                return Expression(globalvar.c_name, globalvar.type_)
        error(f"tried to reference an undeclared identifier `{name}`", (self.current_file, ctx.start.line, ctx.start.column))
        exit(1)
    
    def visitCallParamList(self, ctx:KaminParser.CallParamListContext):
        return ", ".join([self.visit(expr).c_expr for expr in ctx.expression()])
    
    def visitGenericCallParamList(self, ctx:KaminParser.GenericCallParamListContext):
        if ctx.typeName():
            return [self.visit(type_) for type_ in ctx.typeName()]
        else:
            return []

    def visitFuncCall(self, ctx:KaminParser.FuncCallContext):
        name = ".".join([ident.getText() for ident in ctx.Ident()])
        
        if ctx.genericCallParamList():
            found_func = None
            for func in self.generic_functions:
                if func.name == name:
                    found_func = func
            if not found_func:
                error(f"tried to call an unknown generic function `{name}`")
                exit(1)

            generic_params = self.visit(ctx.genericCallParamList())
            exprs = [self.visit(call_param) for call_param in ctx.callParamList().expression()]
            for i, generic_param in enumerate(generic_params):
                if i >= len(found_func.type_.generic_names):
                    error("too many generic parameters were provided", 
                          (self.current_file, ctx.start.line, ctx.start.column))
                if found_func.type_.return_type.base_name == found_func.type_.generic_names[i]:
                    found_func.type_.return_type = generic_param

                for param in found_func.type_.params:
                    if param.type_.base_name == found_func.type_.generic_names[i]:
                        param.type_ = generic_param

            self.generic_type_map.clear()
            for i, generic_param in enumerate(generic_params):
                type_name = found_func.type_.generic_names[i]
                self.generic_type_map[type_name] = generic_param

            func_type_text_params = ", ".join([param.type_.text + " " + param.name for param in found_func.type_.params])
            func_type_text = f"({func_type_text_params}) -> {found_func.type_.text}"
            func_type = FuncType(func_type_text, found_func.type_.return_type, found_func.type_.params)
            call_text_params = "_".join([param.type_.c_type for param in found_func.type_.params])
            call_name = kamin_to_c_name(name) + f"_{func_type.return_type.c_type}" + f"_{call_text_params}"
            call_name = call_name.replace("*", "ptr")

            for func in self.functions:
                if func.c_name == call_name:
                    params = ""
                    if ctx.callParamList():
                        params = self.visit(ctx.callParamList())
                        return Expression(f"{call_name}({params})", found_func.type_.return_type)

            decl_type_text_params = ", ".join([param.type_.c_type + " " + param.name for param in found_func.type_.params])
            decl = f"{func_type.return_type} {call_name}({decl_type_text_params});"
            defn = ""
            if found_func.body_ctx:
                self.make_scope()
                for param in func_type.params:
                    self.scope_stack[-1][param.name] = LocalVar(param.name, param.type_, None)

                defn = self.visit(found_func.body_ctx)
                defn = decl[:-1] + " {\n" + defn + "\n}"
                self.drop_scope()

            self.functions.append(Function(name, call_name, func_type, decl, defn))
            call_exprs = ", ".join([expr.c_expr for expr in exprs])
            return Expression(f"{call_name}({call_exprs})", found_func.type_.return_type)
        else:
            if "c" in self.directive_stack[-1]:
                params = ""
                if ctx.callParamList():
                    params = self.visit(ctx.callParamList())
                return Expression(f"{name}({params})", self.type_map["any"])
            found_func = None
            for func in self.functions:
                if func.name == name:
                    found_func = func
                if func.name.replace(self.module + ".", "") == name:
                    found_func = func
                    name = kamin_to_c_name(func.name)
            if not found_func:
                error(f"tried to call an unknown function `{name}`")
                exit(1)
            params = ""
            if ctx.callParamList():
                params = self.visit(ctx.callParamList())
        
            if name == "main":
                name = "__main"
            return Expression(f"{name}({params})", found_func.type_.return_type)
   
    # visitBuiltinCall helpers
    def buiiltin_using(self, params):
        if len(params) != 1:
            error("@using() expects only one parameter like `@using(\"some.namespace\")`")
            exit(1)

        namespace = params[0].string
        for func in self.functions:
            if func.name.startswith(namespace):
                new_func = copy.deepcopy(func)
                new_func.name = func.name[len(namespace)+1:]
                self.functions.append(new_func)
        
        for gen_func in self.generic_functions:
            if gen_func.name.startswith(namespace):
                new_gen_func = copy.deepcopy(gen_func)
                new_gen_func.name = gen_func.name[len(namespace)+1:]
                self.generic_functions.append(new_gen_func)
        
        for gv in self.globalvars:
            if gv.name.startswith(namespace):
                new_gv = copy.deepcopy(gv)
                new_gv.name = gv.name[len(namespace)+1:]
                self.globalvars.append(new_gv)
        
        for ct in self.custom_types:
            if ct.name.startswith(namespace):
                new_ct = copy.deepcopy(ct)
                new_ct.name = ct.name[len(namespace)+1:]
                self.custom_types.append(new_ct)
        
        for gct in self.generic_custom_types:
            if gct.name.startswith(namespace):
                new_gct = copy.deepcopy(gct)
                new_gct.name = gct.name[len(namespace)+1:]
                self.generic_custom_types.append(new_gct)

        return Expression("", Type("", "", ""))

    def builtin_import(self, params):
        if len(params) != 1:
            error("@import() expects only one parameter like `@import(\"some_module.kamin\")`")
            exit(1)
        path = source_file_to_module_name(params[0].string)
        input_stream = antlr4.FileStream(path[1])
        lexer = KaminLexer(input_stream)
        tokens = antlr4.CommonTokenStream(lexer)
        parser = KaminParser(tokens)
        tree = parser.file_()
        new_visitor = type(self)(path[1], path[0])
        new_visitor.visit(tree)
        
        self.c_file_string = new_visitor.__str__() + "\n" + self.c_file_string

        for globalvar in new_visitor.globalvars:
            found = False
            for gv in self.globalvars:
                found = gv.name == globalvar.name
            if not found:
                globalvar.text = ""
                self.globalvars.append(globalvar)

        for func in new_visitor.functions:
            found = False
            for fn in self.functions:
                found = fn.name == func.name
            if not found:
                func.defn = ""
                func.decl = ""
                self.functions.append(func)

        for generic_func in new_visitor.generic_functions:
            found = False
            for gf in self.generic_functions:
                found = gf.name == generic_func.name
            if not found:
                self.generic_functions.append(generic_func)
       
        for custom_type in new_visitor.custom_types:
            found = False
            for ct in self.custom_types:
                found = ct.name == custom_type.name
            if not found:
                custom_type.inst_text = ""
                self.custom_types.append(custom_type)

        for generic_custom_type in new_visitor.generic_custom_types:
            found = False
            for gct in self.generic_custom_types:
                found = gct.name == generic_custom_type.name
            if not found:
                self.generic_custom_types.append(generic_custom_type)

        return Expression("", Type("", "", ""))

    def builtin_import_c(self, params):
        if len(params) != 1:
            error("@import_c() expects only one parameter like `@import_c(\"some_module.c\")`")
            exit(1)
        path = source_file_to_module_name(params[0].string)
        with open(path[1], "r") as f:
            buf = f.read()
            self.c_file_string = buf + self.c_file_string
        return Expression("", Type("", "", ""))
    
    def builtin_make_ifdef(self, params):
        if len(params) != 1:
            error("@make_ifdef() expects only one parameter like `@make_ifdef(\"SOME_DEFINE\")`")
            exit(1)
        
        expr = params[0].string
        self.c_file_string += f"#ifdef {expr}\n"
        self.in_macro = True
        return Expression("", Type("", "", ""))
    
    def builtin_make_else(self, params):
        if len(params) != 0:
            error("@make_else() expects zero parameters like `@make_else()`")
            exit(1)
        
        self.c_file_string += f"#else\n"
        self.in_macro = True
        return Expression("", Type("", "", ""))
    
    def builtin_make_endif(self, params):
        if len(params) != 0:
            error("@make_endif() expects zero parameters like `@make_endif()`")
            exit(1)
       
        self.c_file_string += f"#endif\n"
        self.in_macro = False
        return Expression("", Type("", "", ""))
    
    def builtin_make_include(self, params):
        if len(params) != 1:
            error("@make_include() expects only one parameter like `@make_include(\"some_header.h\")`")
            exit(1)

        self.c_file_string += f"#include \"{params[0].string}\"\n"
        return Expression("", Type("", "", ""))
    
    def builtin_make_if(self, params):
        if len(params) != 1:
            error("@make_if() expects only one parameter like `@make_if(\"c preprocessor text\")`")
            exit(1)

        expr = params[0].string
        self.c_file_string += f"#if {expr}\n"
        self.in_macro = True
        return Expression("", Type("", "", ""))

    def visitBuiltinCall(self, ctx:KaminParser.BuiltinCallContext):
        name = ctx.Ident().getText()
        if name not in self.builtin_functions:
            error(f"tried to call an unknown builtin function `{name}`")
            exit(1)

        if ctx.callParamList():
            params = [self.visit(expr) for expr in ctx.callParamList().expression()]
        else:
            params = []
        return self.builtin_functions[name](params)
   
    def visitStringExpression(self, ctx:KaminParser.StringExpressionContext):
        text = ctx.String().getText()
        return StringExpression(text, self.type_map["raw_str"], text[1:-1])
    
    def visitMathExpression(self, ctx:KaminParser.MathExpressionContext):
        op1 = self.visit(ctx.expression()[0])
        op2 = self.visit(ctx.expression()[1])
        operator = ""
        if ctx.Asterisk():
            operator = "*"
        elif ctx.Slash():
            operator = "/"
        elif ctx.Plus():
            operator = "+"
        elif ctx.Minus():
            operator = "-"
        return Expression(f"({op1.c_expr} {operator} {op2.c_expr})", op1.type_)
    
    def visitCmpExpression(self, ctx:KaminParser.CmpExpressionContext):
        op1 = self.visit(ctx.expression()[0])
        op2 = self.visit(ctx.expression()[1])
        operator = ""
        if ctx.Eq():
            operator = "=="
        elif ctx.Ne():
            operator = "!="
        elif ctx.Gt():
            operator = ">"
        elif ctx.Lt():
            operator = "<"
        elif ctx.Ge():
            operator = ">="
        elif ctx.Le():
            operator = "<="
        return Expression(f"({op1.c_expr} {operator} {op2.c_expr})", op1.type_)
    
    def visitSubscriptExpression(self, ctx:KaminParser.SubscriptExpressionContext):
        expr = self.visit(ctx.expression()[0])
        subscript = self.visit(ctx.expression()[1])
        
        type_ = Type(expr.type_.text[1:], expr.type_.base_name, expr.type_.c_type[:-1])
        return Expression(f"({expr.c_expr}[{subscript.c_expr}])", type_)
   
    def visitStructExprPair(self, ctx:KaminParser.StructExprPairContext):
        name = ctx.Ident().getText()
        expr = self.visit(ctx.expression())
        return name, expr

    def visitStructExpression(self, ctx:KaminParser.StructExpressionContext):
        name = ".".join([ident.getText() for ident in ctx.Ident()])

        if ctx.genericCallParamList():
            found_struct = None
            for struct in self.generic_custom_types:
                if struct.name == name:
                    found_struct = struct
                full_name = self.module + "." + name
                if struct.name == full_name:
                    found_struct = struct
            if not found_struct:
                error(f"tried to instantiate a unknown generic struct `{name}`")
                exit(1)
            generic_params = self.visit(ctx.genericCallParamList())
            pairs = [self.visit(pair) for pair in ctx.structExprPair()]

            for i, generic_param in enumerate(generic_params):
                for field in found_struct.fields:
                    if i >= len(found_struct.generic_names):
                        error("too many generic parameters were provided",
                              (self.current_file, ctx.start.line, ctx.start.column))
                        exit(1)
                    if field.type_.base_name == found_struct.generic_names[i]:
                        field.type_ = generic_param

            struct_type_text_fields = ",\n".join([field.type_.text + " " + field.name for field in found_struct.fields]) + ","
            struct_type_text = f"struct {{\n{struct_type_text_fields}\n}}"
            struct_type = StructType(struct_type_text, found_struct.fields, name, "", True)
            text_fields = "_".join([field.type_.c_type for field in found_struct.fields])
            inst_name = kamin_to_c_name(name) + f"_{text_fields}"
            inst_name = inst_name.replace("*", "ptr")

            inst_pairs = ", ".join(["." + pair[0] + " = " + pair[1].c_expr for pair in pairs])
            for type_ in self.custom_types:
                if type_.c_type == inst_name:
                    return Expression(f"({inst_name}){{{inst_pairs}}}")
            self.custom_types.append(struct_type)
            struct_type.c_type = inst_name
            return Expression(f"({inst_name}){{{inst_pairs}}}", struct_type)
        else:
            type_ = None
            if name in self.type_map:
                type_ = self.type_map[name]
            else:
                full_name = self.module + "." + name
                if full_name in self.type_map:
                    type_ = self.type_map[full_name]
                    name = full_name

            if type_ == None:
                error(f"cannot initialize an undeclared struct `{name}`", (self.current_file, ctx.start.line, ctx.start.column))
                exit(1)

            pairs = []
            for pair in ctx.structExprPair():
                field_name, expr = self.visit(pair)
                field_found = None
                for field in type_.fields:
                    if field.name == field_name:
                        field_found = field

                if field_found == None:
                    error(f"struct `{name}` does not have a field `{field_name}`", (self.current_file, ctx.start.line, ctx.start.column))
                    exit(1)

                if field_found.type_.c_type != expr.type_.c_type:
                    error(f"field has type `{field_found.type_.text}` and expression has type `{expr.type_.text}`",
                          (self.current_file, ctx.start.line, ctx.start.column))
                    exit(1)
            pairs.append(f".{field_name} = {expr.c_expr}")
        pairs_text = ", ".join(pairs)
        text = f"({kamin_to_c_name(name)}) {{{pairs_text}}}"
        return Expression(text, type_)
    
    def visitStructAccessExpression(self, ctx:KaminParser.StructAccessExpressionContext):
        expr = self.visit(ctx.expression())
        if not isinstance(expr.type_, StructType):
            error("expression is not of a struct type", (self.current_file, ctx.start.line, ctx.start.column))
            exit(1)
        field_name = ctx.Ident().getText()
        field_found = None
        for field in expr.type_.fields:
            if field.name == field_name:
                field_found = field

        if field_found == None:
            error(f"struct type `{expr.type_.text}` does not have a field named `{field_name}`")
            exit(1)

        return Expression(f"{expr.c_expr}.{field_name}", field_found.type_)

    def visitExpression(self, ctx:KaminParser.ExpressionContext):
        self.directive_stack.append([])
        for directive in ctx.directive():
            self.visit(directive)
        result = None
        if ctx.intExpression():
            result = self.visit(ctx.intExpression())
        elif ctx.floatExpression():
            result = self.visit(ctx.floatExpression())
        elif ctx.emphExpression():
            result = self.visit(ctx.emphExpression())
        elif ctx.identExpression():
            result = self.visit(ctx.identExpression())
        elif ctx.funcCall():
            result = self.visit(ctx.funcCall())
        elif ctx.builtinCall():
            result = self.visit(ctx.builtinCall())
        elif ctx.stringExpression():
            result = self.visit(ctx.stringExpression())
        elif ctx.mathExpression():
            result = self.visit(ctx.mathExpression())
        elif ctx.cmpExpression():
            result = self.visit(ctx.cmpExpression())
        elif ctx.subscriptExpression():
            result = self.visit(ctx.subscriptExpression())
        elif ctx.structExpression():
            result = self.visit(ctx.structExpression())
        elif ctx.structAccessExpression():
            result = self.visit(ctx.structAccessExpression())

        if result == None:
            error(f"TODO: unhandled expression type {rule_get_full_text(ctx)}", (self.current_file, ctx.start.line, ctx.start.column))
            exit(1)
        self.directive_stack.pop()
        return result

    def visitFuncParam(self, ctx:KaminParser.FuncParamContext):
        if ctx.tripleDot():
            return FuncTypeParam("", None, is_va=True)
        else:
            name = ctx.Ident().getText()
            type_ = self.visit(ctx.typeName())
            return FuncTypeParam(name, type_)
    
    def visitFuncTypeParamList(self, ctx:KaminParser.FuncTypeParamListContext):
        return [self.visit(func_param) for func_param in ctx.funcParam()]
   
    def visitGenericParamList(self, ctx:KaminParser.GenericParamListContext):
        return [ident.getText() for ident in ctx.Ident()]

    def visitFuncType(self, ctx:KaminParser.FuncTypeContext):
        if ctx.genericParamList():
            generic_names = self.visit(ctx.genericParamList())
            return_type = self.visit(ctx.typeName())
            params = self.visit(ctx.funcTypeParamList()) if ctx.funcTypeParamList() else []
            type_ = GenericFuncType(rule_get_full_text(ctx), generic_names, return_type, params)
            return type_
        else:
            return_type = self.visit(ctx.typeName())
            params = self.visit(ctx.funcTypeParamList()) if ctx.funcTypeParamList() else []
            type_  = FuncType(rule_get_full_text(ctx), return_type, params)
            return type_

    def visitStructField(self, ctx:KaminParser.StructFieldContext):
        name = ctx.Ident().getText()
        type_ = self.visit(ctx.typeName())
        return StructField(name, type_)

    def visitStructType(self, ctx:KaminParser.StructTypeContext):
        fields = []
        for fieldctx in ctx.structField():
            fields.append(self.visit(fieldctx))
        if ctx.genericParamList():
            generic_names = self.visit(ctx.genericParamList())
            type_ = GenericStructTypeBase(rule_get_full_text(ctx), generic_names, fields)
            return type_
        else:
            return StructTypeBase(rule_get_full_text(ctx), fields)
    
    def visitAtomicType(self, ctx:KaminParser.AtomicTypeContext):
        if ctx.funcType():
            return self.visit(ctx.funcType())
        elif ctx.structType():
            return self.visit(ctx.structType())
        elif ctx.Ident():
            name = ".".join([ident.getText() for ident in ctx.Ident()])
            if ctx.Dollar():
                if name in self.generic_type_map:
                    return self.generic_type_map[name]
                return Type(rule_get_full_text(ctx), name, "")

            if name not in self.type_map:
                if self.module + "." + name not in self.type_map:
                    error(f"tried to reference an unknown type `{name}`")
                    exit(1)
                name = self.module + "." + name
            return self.type_map[name]

        error(f"unhandled context", (self.current_file, ctx.start.line, ctx.start.column))
        exit(1)

    def visitTypeName(self, ctx:KaminParser.TypeNameContext):
        base_type = self.visit(ctx.atomicType())
        for prefix in ctx.typePrefix():
            if prefix.Asterisk():
                base_type.text = "*" + base_type.text
                base_type.c_type += "*"
        return base_type
    
    def visitGlobalDeclBody(self, ctx:KaminParser.GlobalDeclBodyContext):
        return "\n".join([("  " * (len(self.scope_stack)-1))+ self.visit(statement) for statement in ctx.statement()])

    def visitGlobalDecl(self, ctx:KaminParser.GlobalDeclContext):
        self.directive_stack.append([])
        for directive in ctx.directive():
            self.visit(directive)

        name = self.module + "." + ctx.Ident().getText()
        type_ = self.visit(ctx.typeName())

        if isinstance(type_, FuncType):
            # we've got a function
            ident = ctx.Ident().getText()
            if ident == "main":
                c_name = "__main"
                name = "main"
            elif ident == "real_main":
                c_name = "main"
                name = "real_main"
            else:
                c_name = kamin_to_c_name(name)

            existing = None
            for func in self.functions:
                if func.name == name:
                    existing = func

            params = []
            for i, param in enumerate(type_.params):
                params.append(param)
            params_text = ", ".join([param.type_.c_type + " " + param.name if not param.is_va else "..." for param in params])

            extern = "extern " if "foreign" in self.directive_stack[-1] else ""
            if extern != "":
                name = ctx.Ident().getText()
            decl = f"{extern}{type_.return_type} {c_name}({params_text});"
            defn = ""
            if ctx.globalDeclBody():
                self.make_scope()
                for param in type_.params:
                    self.scope_stack[-1][param.name] = LocalVar(param.name, param.type_, None)

                defn = self.visit(ctx.globalDeclBody())
                defn = decl[:-1] + " {\n" + defn + "\n}"
                self.drop_scope()

            if existing and existing.decl != "":
                decl = ""

            new_func = Function(name, c_name, type_, decl, defn)
            self.functions.append(new_func)
            self.directive_stack.pop()
            return new_func.decl + "\n" + new_func.defn
        elif isinstance(type_, GenericFuncType):
            self.generic_functions.append(GenericFunction(name, type_, ctx.globalDeclBody()))
            return self.directive_stack.pop()
        elif isinstance(type_, Type):
            expr = None
            if ctx.expression():
                expr = self.visit(ctx.expression())

            is_const = "const" in self.directive_stack[-1]
            gv = GlobalVar(name, kamin_to_c_name(name), type_, expr, is_const)
            text = gv.text
            if self.in_macro:
                gv.text = ""
            self.globalvars.append(gv)
            self.directive_stack.pop()
            return text
        elif isinstance(type_, StructTypeBase):
            c_type = kamin_to_c_name(name)
            struct = StructType(type_.text, type_.fields, name, c_type, False)
            self.custom_types.append(struct)
            self.type_map[name] = struct
            self.directive_stack.pop()
            return struct.__str__()
        elif isinstance(type_, GenericStructTypeBase):
            self.generic_custom_types.append(GenericStructType(type_.text, type_.generic_names, type_.fields, name))
            return self.directive_stack.pop()

        error("unhandled type case", (self.current_file, ctx.start.line, ctx.start.column))
        exit(1)

    def visitDirective(self, ctx:KaminParser.DirectiveContext):
        self.directive_stack.append(ctx.Ident().getText())

def main():
    global STD_PATH

    parser = argparse.ArgumentParser(
        description="kamin programming language compiler"
    )
    parser.add_argument("source_file")
    parser.add_argument("-std", type=str)
    parser.add_argument("-dump-c", action="store_true", help="write generated C code to a file")
    parser.add_argument("-cc-flags", type=str, help="custom CC flags")
    parser.add_argument("-cc", type=str, help="custom CC executable. default is gcc", const="gcc", nargs="?")
    args = parser.parse_args()

    STD_PATH = args.std
    CC = args.cc

    source_file = args.source_file
    module_name, path = source_file_to_module_name(source_file)

    input_stream = antlr4.FileStream(path)
    lexer = KaminLexer(input_stream)
    tokens = antlr4.CommonTokenStream(lexer)
    parser = KaminParser(tokens)
    tree = parser.file_()
    visitor = Visitor(path, module_name)
    visitor.visit(tree)

    file_name_no_ext = os.path.splitext(os.path.basename(args.source_file))[0]
    if args.dump_c:
        with open(file_name_no_ext + ".c", "w") as f:
            f.write(visitor.__str__())

    # CONFIG

    cc = "gcc"
    cc_flags = [
        "-v",
        "-o", file_name_no_ext,
        # "-Wall", "-Wextra",
        "-xc", "-", # stdin
    ]

    if args.cc_flags:
        cc_flags = args.cc_flags.split("")
        cc_flags.append("-xc")
        cc_flags.append("-")

    p = Popen([cc, *cc_flags], stdout=sys.stdout, stdin=PIPE, stderr=sys.stdout)
    p.communicate(input=visitor.__str__().encode())

if __name__ == "__main__":
    main()
