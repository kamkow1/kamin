parser grammar KaminParser;

options {
    tokenVocab = 'generated/KaminLexer';
}

file: statement*;

statement: globalDecl Semicolon
    |   returnStatement Semicolon
    |   assignment Semicolon
    |   reassignment Semicolon
    |   expression Semicolon
    |   ifStatement
    |   forStatement
    |   breakStatement Semicolon
    |   continueStatement Semicolon
    |   whileStatement
    |   expressionAssignment Semicolon
    ;

expression: directive* (intExpression
    |   floatExpression
    |   identExpression
    |   stringExpression
    |   builtinCall
    |   funcCall
    |   cmpExpression
    |   mathExpression
    |   emphExpression
    |   subscriptExpression
    |   structExpression
    |   structAccessExpression
    )
    ;

emphExpression: OpenParen expression CloseParen;
intExpression: Int (Colon typeName)?;
floatExpression: Float (Colon typeName)?;
identExpression: (Ident Dot)* Ident;
stringExpression: String;
callParamList: expression (Comma expression)*;
genericCallParamList: Dollar OpenSqbr (typeName (Comma typeName)*)? CloseSqbr;
builtinCall: At Ident OpenParen callParamList? CloseParen;
funcCall: (Ident Dot)* Ident genericCallParamList? OpenParen callParamList? CloseParen;
cmpExpression: (Eq | Ne | Gt | Lt | Ge | Le) expression expression;
mathExpression: (Plus | Minus | Asterisk | Slash) expression expression;
subscriptExpression: OpenSqbr  CloseSqbr expression expression;
structExprPair: Ident Assignment expression Comma;
structExpression: genericCallParamList? Ident (Dot Ident)* Dot OpenBracket structExprPair* CloseBracket;
structAccessExpression: Dot expression Ident;

directive: Hash Ident;

assignment: directive* Ident Colon typeName? Colon Assignment expression;
reassignment: (Ident Dot)* Ident Assignment expression;
expressionAssignment: expression Assignment expression;

globalDeclBody: OpenBracket statement* CloseBracket;
globalDecl: directive* Ident Colon typeName (Colon (globalDeclBody | expression?))?;

returnStatement: Return expression?;
ifBody: OpenBracket statement* CloseBracket;
elseBody: Else OpenBracket statement* CloseBracket;
elifBody: Elif expression OpenBracket statement* CloseBracket;
ifStatement: If expression ifBody elifBody* elseBody?;
forBody: OpenBracket statement* CloseBracket;
forStatement: For statement statement statement forBody;
breakStatement: Break;
continueStatement: Continue;
whileBody: OpenBracket statement* CloseBracket;
whileStatement: While expression whileBody;

genericParamList: Dollar OpenSqbr (Ident (Comma Ident)*)? CloseSqbr;
tripleDot: Dot Dot Dot;
funcParam: (typeName Ident) | tripleDot;
funcTypeParamList: funcParam (Comma funcParam)*;
funcType: genericParamList? OpenParen funcTypeParamList? CloseParen Arrow typeName;
structField: typeName Ident Comma;
structType: genericParamList? Struct OpenBracket structField* CloseBracket;
atomicType: Dollar? (funcType | structType | Ident (Dot Ident)*);
typePrefix: Asterisk;
typeName: directive* typePrefix* atomicType;

